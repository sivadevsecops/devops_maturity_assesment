from django.db import models
from django.urls import reverse


# Create your models here.
class Grade(models.Model):
    name = models.CharField(max_length=100)
    score = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Question(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    description = models.TextField()
    weight = models.FloatField(default=1.0)

    def __str__(self):
        return self.description


class Answer(models.Model):
    grade = models.ForeignKey(Grade, on_delete=models.PROTECT)
    question = models.ForeignKey(Question, on_delete=models.PROTECT)
    description = models.TextField()

    def __str__(self):
        return self.description


class Area(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class AssessmentTemplate(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    version = models.FloatField(default=1.0)
    name = models.CharField(max_length=100)
    categories = models.ManyToManyField(Category)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'version'], name="assessment template")
        ]

    def __str__(self):
        return f'{self.name} - V{self.version}'


class Assessment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    assessment_template = models.ForeignKey(AssessmentTemplate, on_delete=models.PROTECT)
    answers = models.ManyToManyField(Answer)

    def __str__(self):
        return f'{self.name} - {self.created.date()}'

    def get_absolute_url(self):
        return reverse('assessment_view', args=[self.id])
