# Generated by Django 3.1.6 on 2021-02-10 00:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0007_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='measure',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='assessments.category'),
        ),
    ]
