# Generated by Django 3.1.6 on 2021-02-10 00:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0005_auto_20210209_2306'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Area',
        ),
        migrations.AddField(
            model_name='assessment',
            name='area',
            field=models.CharField(default='MNP', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='measure',
            name='category',
            field=models.CharField(max_length=100),
        ),
        migrations.DeleteModel(
            name='Category',
        ),
    ]
